<?php

namespace Project\Articles;

use Doctrine\ORM\Mapping as ORM;
use Etten\Doctrine\Entities;
use Project\Routes;

/**
 * @ORM\Entity()
 */
class Article extends Entities\UuidBinaryEntity
{

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime")
	 */
	private $created;

	/**
	 * @var string
	 * @ORM\Column(type="string")
	 */
	private $name = '';

	/**
	 * @var string
	 * @ORM\Column(type="text")
	 */
	private $content = '';

	/**
	 * @var Routes\Route
	 * @ORM\OneToOne(targetEntity="\Project\Routes\Route", cascade={"persist", "remove"})
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $route;

	public function __construct(\DateTime $created = NULL)
	{
		parent::__construct();
		$this->created = $created ?: new \DateTime();
	}

	public function getCreated(): \DateTime
	{
		return $this->created;
	}

	public function getName(): string
	{
		return $this->name;
	}

	public function setName(string $name)
	{
		$this->name = $name;
	}

	public function getContent(): string
	{
		return $this->content;
	}

	public function setContent(string $content)
	{
		$this->content = $content;
	}

	public function getRoute(): Routes\Route
	{
		return $this->route;
	}

	public function setRoute(Routes\Route $route)
	{
		$this->route = $route;
	}

}
