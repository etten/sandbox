<?php

namespace App\Front\Presenters;

use Project\Articles;

class BlogPresenter extends BasePresenter
{

	/**
	 * @var Articles\Articles
	 * @inject
	 */
	public $articles;

	public function renderDefault()
	{
		$this->template->articles = $this->articles->findAll();
	}

}
