<?php

namespace App\Forms;

use Nette\Application\UI\Form;
use Nette\SmartObject;

class FormFactory
{

	use SmartObject;

	/** @var callable[] */
	public $onCreate = [];

	public function create(): Form
	{
		$form = new Form;
		$this->onCreate($form);

		return $form;
	}

}
