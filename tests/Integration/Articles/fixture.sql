CREATE TABLE IF NOT EXISTS `route` (
	`id` BINARY(16) NOT NULL
	COMMENT '(DC2Type:uuid_binary)',
	`url` LONGTEXT
	COLLATE utf8_unicode_ci NOT NULL,
	`url_hash` VARCHAR(32)
	COLLATE utf8_unicode_ci NOT NULL,
	`type` VARCHAR(255)
	COLLATE utf8_unicode_ci NOT NULL,
	`title` VARCHAR(255)
	COLLATE utf8_unicode_ci NOT NULL,
	`keywords` VARCHAR(255)
	COLLATE utf8_unicode_ci NOT NULL,
	`description` LONGTEXT
	COLLATE utf8_unicode_ci NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `UNIQ_2C42079CFECAB00` (`url_hash`)
)
	ENGINE = InnoDB
	DEFAULT CHARSET = utf8
	COLLATE = utf8_unicode_ci;

CREATE TABLE article (
	id BINARY(16) NOT NULL
	COMMENT '(DC2Type:uuid_binary)',
	route_id BINARY(16) NOT NULL
	COMMENT '(DC2Type:uuid_binary)',
	created DATETIME NOT NULL,
	name VARCHAR(255) NOT NULL,
	content LONGTEXT NOT NULL,
	UNIQUE INDEX UNIQ_23A0E6634ECB4E6 (route_id),
	PRIMARY KEY (id)
)
	DEFAULT CHARACTER SET utf8
	COLLATE utf8_unicode_ci
	ENGINE = InnoDB;
ALTER TABLE article
	ADD CONSTRAINT FK_23A0E6634ECB4E6 FOREIGN KEY (route_id) REFERENCES route (id);
