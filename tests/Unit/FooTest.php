<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use Project\Foo;

class FooTest extends TestCase
{

	public function testGetBar()
	{
		$foo = new Foo();
		$this->assertSame('bar', $foo->getBar());
	}

}
