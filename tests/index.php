<?php

namespace Tests;

use Etten;

/** @var Etten\App\App $app */
$app = require __DIR__ . '/bootstrap.php';
$app->run();
