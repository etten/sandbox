before_script:
  # Run daemons
  - service mysql start

  # Check environment
  - php -v
  - mysql -V
  - composer -V
  - nodejs -v

#  # SSH keys
#  - eval $(ssh-agent -s)
#  - ssh-add <(cat .ssh/id_rsa)
#
#  # SSH: By-pass Host key verification failed.
#  - mkdir -p ~/.ssh
#  - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'

  # Set file/directory permissions
  - chmod -R 777 var
  - chmod -R 777 tests/var

  # Set config files for CI/CD
  - cp tests/config.ci.neon app/config/config.local.neon
  - cp tests/config.ci.neon tests/config.local.neon

  # Database
  - mysql -e 'create database test;'

stages:
  - build
  - test
  - deploy

build:
  stage: build
  image: phpdocker/phpdocker:7.2
  artifacts:
    paths:
      # artifact untracked files
      - node_modules/
      - vendor/
      - web/bower/

      # artifact compiled files (just for a build)
      - web/js/
      - web/css/
    expire_in: 12 hours
  script:
    # Composer
    - composer install --no-interaction --prefer-dist --optimize-autoloader

    # Bower
    - bower install --allow-root --config.interactive=false

    # NodeJS
    - npm install
    - grunt production

test:
  stage: test
  image: phpdocker/phpdocker:7.2
  artifacts:
    paths:
      - var/log/
      - tests/var/log/
    when: on_failure
    expire_in: 6 months
  dependencies:
    - build
  script:
    # Run PHP CodeSniffer
    - composer run-script cs

    # Run PHPUnit
    - composer run-script tests

    # Run Migrations
    - composer run-script migrations

stage:
  stage: deploy
  image: phpdocker/phpdocker:7.2
  environment: staging
  only:
    - stage
  artifacts:
    paths:
      - var/log/
      - tests/var/log/
    when: on_failure
    expire_in: 6 months
  dependencies:
    - build
  script:
    # Deploy it
    - composer run-script deploy-staging

    # Check HTTP status
    - URL="https://www.example.com" check-status-code

production:
  stage: deploy
  image: phpdocker/phpdocker:7.2
  environment: production
  only:
    - master
  when: manual
  artifacts:
    paths:
      - var/log/
      - tests/var/log/
    when: on_failure
    expire_in: 6 months
  dependencies:
    - build
  script:
    # Deploy it
    - composer run-script deploy-production

    # Check HTTP status
    - URL="https://www.example.com" check-status-code

cache:
  key: "$CI_PROJECT_PATH-$CI_COMMIT_REF_SLUG"
  paths:
    # cache untracked files only (shared key)
    - node_modules/
    - vendor/
    - web/bower/
