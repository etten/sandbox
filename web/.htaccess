# Apache configuration file (see httpd.apache.org/docs/current/mod/quickreference.html)

# Enable cool URL
<IfModule mod_rewrite.c>
	RewriteEngine On

    # Deterine localhost to avoid some further actions.
	RewriteCond %{HTTP_HOST} ^localhost [NC]
	RewriteRule ^(.*) - [E=LOCALHOST:true]

    # Determine secured connection.
	<IfModule mod_ssl.c>
        RewriteCond %{HTTPS} =on [OR]
    </IfModule>
    RewriteCond %{ENV:HTTPS} =on [OR]
    RewriteCond %{HTTP:X-Forwarded-Proto} =https
	RewriteRule ^(.*) - [E=HTTPS:true]

	# We use rewrite of all from root to /web
	RewriteCond %{ENV:REDIRECT_STATUS} !200
	RewriteCond %{REQUEST_URI} ^/web/
	RewriteRule (.*) /$1 [R=301,L,QSA,NE]

    # Force HTTPS
	RewriteCond %{ENV:HTTPS} !=true
	RewriteCond %{ENV:LOCALHOST} !=true
	RewriteRule ^(.*)$ https://%{HTTP_HOST}/$1 [R=301,L,QSA,NE]

	# Redirect to www
	RewriteCond %{HTTP_HOST} ^([^.]+)\.([^.]+)$ [NC]
	RewriteRule (www)?(.*) https://www.%{HTTP_HOST}/$2 [R=301,L,QSA,NE]

#	# Redirect to non-www
#	RewriteCond %{HTTP_HOST} ^www\.(.+)$ [NC]
#	RewriteRule (.*) https://%1/$1 [R=301,L,QSA,NE]

	# Prevents files starting with dot to be viewed by browser
	RewriteRule /\.|^\. - [F]

	# Front controller
	RewriteCond %{REQUEST_FILENAME} !-f
	RewriteRule .* index.php [L]
</IfModule>

# Disable directory listing
<IfModule mod_autoindex.c>
	Options -Indexes
</IfModule>

# Security headers
# Be aware of PHP as FastCGI where these headers have to be set in PHP script.
<IfModule mod_headers.c>
    # HSTS for 31536000 seconds = 1 year
    Header always set Strict-Transport-Security "max-age=31536000; includeSubDomains; preload" env=HTTPS

    Header always set X-Frame-Options "SAMEORIGIN"
    Header always set X-XSS-Protection "1; mode=block"
    Header always set X-Content-Type-Options "nosniff"
    Header always set Referrer-Policy "strict-origin"

#    # Content-Security-Policy
#    Header always set Content-Security-Policy "default-src 'self';"

#    # Allow custom origin for additional resources
#    SetEnvIf Origin "^http(s)?://(www\.)?(example\.com|example\.org)$" AccessControlAllowOrigin=$0
#    Header always set Access-Control-Allow-Origin %{AccessControlAllowOrigin}e env=AccessControlAllowOrigin
</IfModule>

# Define mime types
<IfModule mod_mime.c>
	AddType text/plain .txt
	AddType text/html .html .htm
	AddType text/xsd .xsd
	AddType text/xsl .xsl
	AddType text/xml .xml

	AddType text/css .css
	AddType text/x-component .htc
	AddType application/x-javascript .js
	AddType application/json .json

	AddType image/svg+xml .svg .svgz
	AddType image/x-icon .ico
	AddType image/bmp .bmp
	AddType image/gif .gif
	AddType image/jpeg .jpg .jpeg .jpe
	AddType image/png .png
	AddType image/tiff .tif .tiff

	AddType application/vnd.ms-fontobject .eot
	AddType application/x-font-otf .otf
	AddType application/x-font-ttf .ttf .ttc
	AddType application/x-font-woff .woff

	AddType application/x-gzip .gz .gzip
	AddType application/pdf .pdf
	AddType application/x-shockwave-flash .swf
</IfModule>

# Enable gzip compression
<IfModule mod_deflate.c>
	# Common types
	AddOutputFilterByType DEFLATE text/html text/plain text/xml text/css text/x-component
	AddOutputFilterByType DEFLATE application/json application/xml application/xhtml+xml application/atom+xml
	AddOutputFilterByType DEFLATE application/javascript application/x-javascript text/ecmascript text/javascript

	# Images
	AddOutputFilterByType DEFLATE image/vnd.microsoft.icon image/x-icon image/bmp image/tiff
	AddOutputFilterByType DEFLATE application/pdf

	# Fonts
	AddOutputFilterByType DEFLATE application/x-font-otf application/x-font-ttf application/vnd.ms-fontobject

	# New types (in registration process)
	AddOutputFilterByType DEFLATE application/xslt+xml image/svg+xml

	# Drop problematic browsers
	BrowserMatch ^Mozilla/4 gzip-only-text/html
	BrowserMatch ^Mozilla/4\.0[678] no-gzip
	BrowserMatch \bMSI[E] !no-gzip !gzip-only-text/html
</IfModule>

# Set browser caching rules
<IfModule mod_expires.c>
	ExpiresActive on

	# RSS
	ExpiresByType application/rss+xml "access plus 1 hour"

	# Assets
	ExpiresByType text/css "access plus 1 month"
	ExpiresByType application/x-javascript "access plus 1 month"
	ExpiresByType text/x-component "access plus 1 month"

	# Images
	ExpiresByType image/svg+xml "access plus 1 week"
	ExpiresByType image/x-icon "access plus 1 week"
	ExpiresByType image/bmp "access plus 1 week"
	ExpiresByType image/gif "access plus 1 week"
	ExpiresByType image/jpeg "access plus 1 week"
	ExpiresByType image/png "access plus 1 week"
	ExpiresByType image/tiff "access plus 1 week"

	# Fonts
	ExpiresByType application/vnd.ms-fontobject "access plus 1 week"
	ExpiresByType application/x-font-otf "access plus 1 week"
	ExpiresByType application/x-font-ttf "access plus 1 week"
	ExpiresByType application/x-font-woff "access plus 1 week"
</IfModule>
